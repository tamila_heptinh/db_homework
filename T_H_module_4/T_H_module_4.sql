USE T_H_module_3
GO

SELECT *
FROM [T_H_module_3_schema].product
GO

--�������� ������ ��� ������� UPDATE, ������ �������� updated_date.
CREATE OR ALTER TRIGGER trg_Update
ON [T_H_module_3_schema].[product]
FOR UPDATE
AS
    UPDATE[T_H_module_3_schema].[product]
    SET updated_date = GETDATE()
    WHERE id IN (SELECT DISTINCT ID FROM Inserted)
GO

--�������� ������������� ��� ����� �������.
CREATE OR ALTER VIEW [T_H_module_3_schema].[product_view]
with schemabinding
AS 
SELECT [product_name]
      ,[description]
      ,[unit_price]
      ,[inserted_date]
      ,[updated_date]
  FROM [T_H_module_3_schema].[product]
GO

CREATE OR ALTER VIEW [T_H_module_3_schema].[supplier_view]
with schemabinding
AS 
SELECT [first_name]
      ,[last_name]
      ,[gender]
      ,[date_of_birth]
      ,[phone]
      ,[address]
  FROM [T_H_module_3_schema].[supplier]
GO

--�������� ������������� � check_option.
create OR alter view T_H_module_3_schema.vw_supplier
with schemabinding
as 
select first_name
      ,last_name
      ,gender
      ,date_of_birth
  from T_H_module_3_schema.supplier
  where gender = 'm'
  with check option
GO

--�������� ���� ������� 
create table T_H_module_3_schema.[order]
		(id INT IDENTITY(1,1) NOT NULL,
		customer_id INT NOT NULL,
		order_number INT NOT NULL,
		operation_type VARCHAR(50) NOT NULL,
		operation_date date NULL,	 
		[status] varchar(50) NOT NULL, --status can be 'shipped', 'in progress' or 'canceled'
		shipped_date date NOT NULL,
		delivery_date date NOT NULL,
		price decimal(18,4) NOT NULL,
		delivery_address_line1 varchar(100) NOT NULL,
		delivery_address_line2 varchar(100) NULL DEFAULT 'Not available',
		)
go

alter table  T_H_module_3_schema.[order]
add constraint PK_order PRIMARY KEY (id)
go

create table T_H_module_3_schema.customer
		(id INT IDENTITY(1,1) NOT NULL,
		first_name nvarchar(50) NOT NULL,
		last_name nvarchar(50) NOT NULL,
		phone_number varchar(15) NOT NULL,
		[address] varchar(100) NOT NULL,
		city varchar(50) NOT NULL,
		country varchar(50) NOT NULL
		)
go

alter table  T_H_module_3_schema.customer
add constraint PK_customer PRIMARY KEY (id)
go

alter table  T_H_module_3_schema.[order]
add constraint FK_customer_order foreign key (customer_id)
references  T_H_module_3_schema.customer(id)
on delete cascade
on update cascade
go

--��� ������� ������� �������� ��� ������ (��� �������� ������, ������, ����).
--INSERT trigger
drop trigger if exists trg_order_date_ins
go

create trigger trg_order_date_ins on T_H_module_3_schema.[order]
after insert 
as
begin
    update T_H_module_3_schema.[order]
        set operation_date = GETDATE()
        from [order] o JOIN --instead of join we can use WHERE id IN (SELECT DISTINCT id FROM Inserted)
             Inserted i
        on o.id = i.id;
end;
go

--UPDATE trigger
drop trigger if exists trg_order_number_upd
go

create trigger trg_order_number_upd on T_H_module_3_schema.[order]
after update 
as
if update(order_number)
begin
       PRINT 'Update error, you cannot change order number'
       ROLLBACK TRANSACTION
END
go

-- DELETE trigger. This trigger doesn't allow to delete orders that haven't been shipped or canceled (or which are in progress)
drop trigger if exists trg_order_del
go

create trigger trg_order_del on T_H_module_3_schema.[order]
for delete 
as
if exists (SELECT * FROM deleted where [status] like 'in progress')
begin
       PRINT 'Delete error, you cannot delete an order which is in progress'
       ROLLBACK TRANSACTION
END
go

--��� 2 ������ �������� ���������� ������ 
SELECT [id]
      ,[first_name]
      ,[last_name]
      ,[phone_number]
      ,[address]
      ,[city]
      ,[country]
  FROM [T_H_module_3_schema].[customer]
GO

SELECT [id]
      ,[customer_id]
      ,[order_number]
      ,[operation_type]
      ,[operation_date]
      ,[status]
      ,[shipped_date]
      ,[delivery_date]
      ,[price]
      ,[delivery_address_line1]
      ,[delivery_address_line2]
  FROM [T_H_module_3_schema].[order]
GO

--INSERT trigger works

--UPDATE trigger (changing order number)
UPDATE [T_H_module_3_schema].[order]
   SET [order_number] = 32487239
 WHERE customer_id = 1
GO
/*RESULT Update error, you cannot change order number
Msg 3609, Level 16, State 1, Line 163
The transaction ended in the trigger. The batch has been aborted.
*/


--DELETE trigger (deleting orders in progress)
DELETE FROM [T_H_module_3_schema].[order]
      WHERE [status] = 'in progress'
GO
/*RESULT Delete error, you cannot delete an order which is in progress
Msg 3609, Level 16, State 1, Line 169
The transaction ended in the trigger. The batch has been aborted.
*/

--��� ������� � �������� 2 � 3 �������� �������� � ��� education 
create database education
go

create schema T_Heptinh
go

use education
go

create synonym T_Heptinh.[order] for T_H_module_3.[T_H_module_3_schema].[order]
go

select * from T_Heptinh.[order]
go

create synonym T_Heptinh.[customer] for T_H_module_3.[T_H_module_3_schema].[customer]
go

select * from T_Heptinh.[customer]
go

create synonym T_Heptinh.[product] for T_H_module_3.[T_H_module_3_schema].[product]
go

select * from T_Heptinh.[product]
go

create synonym T_Heptinh.[supplier] for T_H_module_3.[T_H_module_3_schema].[supplier]
go

select * from T_Heptinh.[supplier]
go