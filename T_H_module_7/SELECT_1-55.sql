use labor_sql
go

--1
Select distinct maker, [type] 
from product 
where type='laptop'
order by maker
go

--2
Select model, ram, screen, price 
from laptop 
where price>1000
order by ram asc, price desc 
go

--3
Select code, model, color, [type], price 
from printer 
where color='y'
order by price desc
go

--4
Select model, speed, hd, cd, price 
from pc 
where price<600 and(cd='12x' or cd='24x')
order by speed desc
go

--5
select distinct [name], class
from ships
order by [name]
go

--6
select code, model, speed, ram, hd, cd, price
from pc
where speed >= 500 and price < 800
order by price desc
go

--7
select code, model, color, [type], price
from printer
where [type] != 'Matrix' and price < 300
order by [type] desc

--8
select model, speed
from pc
where price between 400 and 600
order by hd
go

--9
select model, speed, hd, price
from laptop
where screen >= 12
order by price desc
go

--10
select model, [type], price
from printer
where price < 300
order by [type] desc
go

--11
select model, ram, price
from laptop
where ram = 64
order by screen
go

--12
select model, ram, price
from laptop
where ram > 64
order by hd
go

--13
select model, speed, price
from pc
where speed between 500 and 750
order by hd desc
go

--14
select point, [date], [out]
from outcome_o
where [out] > 2000
order by [date] desc
go

--15
select point, [date], inc
from income_o
where [inc] between 5000 and 10000
order by [date] desc
go

--16
select code, point, [date], inc
from income
where point = 1
order by inc
go

--17
select code, point, [date], [out]
from outcome
where point = 2
order by [out]
go

--18
select class, [type], country, numGuns, bore, displacement
from classes
where country = 'Japan'
order by [type] desc
go

--19
select [name], launched
from ships
where launched between 1920 and 1942
order by launched desc
go

--20
select ship, battle, result
from outcomes
where battle = 'Guadalcanal' and result != 'sunk'
order by ship desc
go

--21
select ship, battle, result
from outcomes
where result = 'sunk'
order by ship desc
go

--22
select class, displacement
from classes
where displacement >= 40
order by [type]
go

--23
select trip_no, town_from, town_to
from trip
where town_from = 'London' or town_to = 'London'
order by  time_out
go

--24
select trip_no, plane, town_from, town_to
from trip
where plane = 'TU-134'
order by  time_out desc
go

--25
select trip_no, plane, town_from, town_to
from trip
where plane <> 'IL-86'
order by plane
go

--26
select trip_no, town_from, town_to
from trip
where town_from != 'Rostov' and town_to != 'Rostov'
order by  plane
go

--27
select model
from pc
where model like '%1%1%'
go

--28
select code, point, [date], [out]
from outcome
where month(date) = 03
go

--29
select point, [date], [out]
from Outcome_o
where day(date) = 14
go

--30
select [name]
from ships
where [name] like 'W%n'
go

--31
select [name]
from ships
where [name] like '%[eE]%[eE]'
go

--32
select [name], launched 
from ships
where [name] not like '%a'
go

--33
select [name]
from ships
where [name] like '% %' and [name] not like '% %c'
go

--34
select trip_no, id_comp, plane, town_from, town_to, time_out, time_in
from trip
where datepart(hh, time_out) between 12 and 17
go

--35
select trip_no, id_comp, plane, town_from, town_to, time_out, time_in
from trip
where datepart(hh, time_in) between 17 and 23
go

--36
select trip_no, time_in 
from trip
where datepart(hour, time_in) between 21 and 0
		or  datepart(hour, time_in) between 00 and 10
go

--37
select [date]
from pass_in_trip
where place like '1%'
go

--38
select [date]
from pass_in_trip
where place like '%c'
go

--39
select [name]
from passenger
where [name] like '% C%'
go

--40
select [name]
from passenger
where [name] not like '% J%'
go

--41
declare @avg_price as int
select @avg_price = avg(price)
from laptop
print '������� ���� ' + CONVERT(VARCHAR(10), @avg_price)
go

--42(1)
select ' Code = ' + cast(code as varchar) + ' Model = ' + model + ' Speed = ' + cast(speed as varchar) + ' Ram = ' +  cast(ram as varchar) + ' HD = ' + cast(hd as varchar) + ' Price = ' + cast(price as varchar) + ' Screen = ' + cast(screen as varchar) 
from laptop
go

--42(2) with CURSOR
declare @row_num int, @code int, @model varchar(50), @speed smallint, @ram smallint, @hd real, @price decimal(8,2), @screen tinyint
set @row_num = 1
declare crs_laptop cursor 
for select code, model, speed, ram, hd, price, screen from laptop
open crs_laptop
fetch next from crs_laptop into @code, @model, @speed, @ram, @hd, @price, @screen
while @@FETCH_STATUS = 0
begin
	print cast(@row_num as varchar) + ' Code = ' + cast(@code as varchar) + ' Model = ' + @model + ' Speed = ' + cast(@speed as varchar) + ' Ram = ' +  cast(@ram as varchar) + ' HD = ' + cast(@hd as varchar) + ' Price = ' + cast(@price as varchar) + ' Screen = ' + cast(@screen as varchar)
	set @row_num = @row_num + 1
	fetch next from crs_laptop into @code, @model, @speed, @ram, @hd, @price, @screen
end
close crs_laptop
deallocate crs_laptop
go

--43(1)
select cast(year([date]) as varchar) + '.0' + cast(month([date]) as varchar) + '.' + cast(day([date]) as varchar) 
from income
go

--43(2)with CURSOR
declare @row_num int, @date datetime
set @row_num = 1
declare crs_income cursor 
for select [date] from income
open crs_income
fetch next from crs_income into @date
while @@FETCH_STATUS = 0
begin
	print cast(year(@date) as varchar) + '.' + cast(month(@date) as varchar) + '.' + cast(day(@date) as varchar)
	set @row_num = @row_num + 1
	fetch next from crs_income into @date
end
close crs_income
deallocate crs_income
go

--44(1)
select ship, battle, 
	case result
		when 'sunk' then '����������'
		when 'damaged' then '�����������'
		else '�����'
	end
from outcomes

--44(2)with CURSOR
declare @row_num int, @ship varchar(50), @battle varchar(50), @result varchar(50)
set @row_num = 1
declare crs_outcomes cursor 
for select ship, battle, result from outcomes
open crs_outcomes
fetch next from crs_outcomes into @ship, @battle, @result
while @@FETCH_STATUS = 0
begin
		if @result = 'sunk' set @result = '����������'
		else if @result = 'damaged' set @result = '�����������'
		else set @result = '�����'
	print @ship +' ' + @battle +  ' ' + @result
	set @row_num = @row_num + 1
	fetch next from crs_outcomes into @ship, @battle, @result
end
close crs_outcomes
deallocate crs_outcomes
go

--45(1)
select '���: ' + cast(substring(place, 1, 1) as varchar) + ' ̳���: ' + cast(substring(place, 2, 2) as varchar) as place
from pass_in_trip
go

--45(2)with CURSOR
declare @row_num int, @place varchar(10)
set @row_num = 1
declare crs_pass_in_trip cursor 
for select place from pass_in_trip
open crs_pass_in_trip
fetch next from crs_pass_in_trip into @place
while @@FETCH_STATUS = 0
begin
	print '���: ' + cast(substring(@place, 1, 1) as varchar) + ' ̳���: ' + cast(substring(@place, 2, 2) as varchar)
	set @row_num = @row_num + 1
	fetch next from crs_pass_in_trip into @place
end
close crs_pass_in_trip
deallocate crs_pass_in_trip
go

--46(1)
select trip_no, id_comp, plane, concat(' from ', town_from) as town_from, concat(' to ',town_to) as town_to, time_out, time_in
from trip

--46(2)with CURSOR
declare @row_num int, @trip_no int, @id_comp int, @plane char(10), @town_from char(25), @town_to char(25), @time_out datetime, @time_in datetime
set @row_num = 1
declare crs_trip cursor 
for select trip_no, id_comp, plane, town_from, town_to, time_out, time_in from trip
open crs_trip
fetch next from crs_trip into @trip_no, @id_comp, @plane, @town_from, @town_to, @time_out, @time_in 
while @@FETCH_STATUS = 0
begin
	print cast(@trip_no as varchar) + ' ' + cast(@id_comp as varchar) + ' ' + @plane + ' from ' + @town_from + ' to ' + @town_to + cast(@time_out as varchar) + ' ' + cast(@time_in as varchar)
	set @row_num = @row_num + 1
	fetch next from crs_trip into @trip_no, @id_comp, @plane, @town_from, @town_to, @time_out, @time_in 
end
close crs_trip
deallocate crs_trip
go

--47
select trip_no,id_comp,plane, town_from, town_to, time_out, time_in, cast(left(trip_no, 1) as varchar) + cast(right(trip_no, 1) as varchar) + cast(id_comp as varchar) + cast(left(plane, 1) as varchar) + cast(right(plane, 1) as varchar) + cast(left(town_from, 1) as varchar) + cast(right(town_from, 1) as varchar) + cast(left(town_to, 1) as varchar) + cast(right(town_to, 1) as varchar) + LEFT(CONVERT(CHAR(19),time_out,102),1) + LEFT(CONVERT(CHAR(19),time_out,102),1) + LEFT(CONVERT(CHAR(19),time_in,102),1) + right(CONVERT(CHAR(19),time_in,102),1) as col
from trip
go

--48
select maker, count(distinct model) as num_models
from product
group by maker
having count(distinct model) >= 2
go

--49
select town, count(trip_no) num from 
(select trip_no,town_from as town from trip
union
select trip_no,town_to as town from trip) as a
group by town

--50
select [type], count(model) as num_models
from printer
group by [type]
go

--51
select distinct model, speed, count(distinct cd) as cd_sum, count(distinct model) as model_sum
from pc
group by grouping sets(model, speed)

--52
select trip_no, time_out, time_in,
	case 
		when datediff(hour, time_out, time_in) < 0 then datediff(hour, time_out, dateadd(hour, 24, time_in)) 
		else datediff(hour, time_out, time_in)
	end as duration
from trip

--53
select point, [date], sum(out) as sum_out, min(out) as min_out, max(out) as max_out
from outcome
group by point, [date] with rollup

--54
select trip_no, substring(place, 1, 1) [row], count(place) as num_places
from pass_in_trip
group by trip_no, substring(place, 1, 1)
order by trip_no
go

--55
select count([name]) as num_passengers
from passenger
where [name] like '% S%'
union all
select count([name]) as num_passengers
from passenger
where [name] like '% B%'
union all
select count([name]) as num_passengers
from passenger
where [name] like '% A%'

