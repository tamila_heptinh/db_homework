CREATE DATABASE [T_H_module_3];
GO

CREATE SCHEMA [T_H_module_3_schema];
GO

CREATE TABLE [T_H_module_3_schema].[product](
	[id] int IDENTITY(1,1) NOT NULL,
	[product_name] nvarchar(50) NOT NULL,
	[supplier_id] int NOT NULL,
	[image] varbinary(max) NULL,
	[description] nvarchar(max) NULL DEFAULT 'No description available',
	[unit_cost] decimal(19, 4) NOT NULL CHECK (unit_cost > 0), --unit_cost, unit_price and unit_weight must not be negative values
	[unit_price] decimal(19, 4) NOT NULL CHECK (unit_price > 0),
	[unit_weight] decimal(19, 4) NOT NULL CHECK (unit_weight > 0),
	[quantity_per_unit] int NOT NULL,
	[inserted_date] date DEFAULT GETDATE(),
	[updated_date] date	NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED ([id]))
 GO

ALTER TABLE [T_H_module_3_schema].[product]
ADD CONSTRAINT [U_product_name] UNIQUE ([product_name]);

GO
CREATE TABLE [T_H_module_3_schema].[supplier](
	[id] int IDENTITY(1,1) NOT NULL,
	[first_name] nvarchar(50) NOT NULL,
	[last_name] nvarchar(50) NOT NULL,
	[gender] char(1) NOT NULL, --it can be either 'm' of 'f'
	[date_of_birth] date NOT NULL,
	[phone] varchar(50) NOT NULL,
	[email] varchar(50) NULL DEFAULT 'No email address available',
	[fax] varchar(50) NULL DEFAULT 'No fax available',
	[address] nvarchar(100) NOT NULL,
	[city] nvarchar(50) NOT NULL,
	[country] nvarchar(50) NOT NULL
 CONSTRAINT [PK_supplier] PRIMARY KEY CLUSTERED ([id]))
 GO

ALTER TABLE [T_H_module_3_schema].[product]  
WITH CHECK ADD  CONSTRAINT [FK_product_supplier] FOREIGN KEY([supplier_id])
REFERENCES [T_H_module_3_schema].[supplier] ([id])
GO

USE [T_H_module_3];

INSERT INTO [T_H_module_3_schema].[supplier]
           ([first_name]
           ,[last_name]
           ,[gender]
           ,[date_of_birth]
           ,[phone]
           ,[email]
           ,[address]
           ,[city]
           ,[country])
     VALUES
           ('�����'
           ,'������'
           ,'f'
           ,'1995-06-12'
           ,'+380467282845'
           ,'hves.tamila@gmail.com'
           ,'�����'
           ,'����'
           ,'������');
GO

INSERT INTO [T_H_module_3_schema].[product]
           ([product_name]
           ,[description]
		   ,[supplier_id]
           ,[unit_cost]
           ,[unit_price]
           ,[unit_weight]
           ,[quantity_per_unit]
		   ,[updated_date])
     VALUES
           ('Apples'
           ,'A package of apples'
		   ,4
           ,150.5
           ,170.5
           ,50
           ,70
           ,'2018-06-12')
GO

SELECT *
FROM [T_H_module_3_schema].[supplier]

SELECT *
FROM [T_H_module_3_schema].[product]