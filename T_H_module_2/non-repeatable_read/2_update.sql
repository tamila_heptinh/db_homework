use AdventureWorks2016CTP3
go

begin transaction

	update [Person].[Person] 
	set [LastName] = 'Lee' 
	where [BusinessEntityID] = 5

commit transaction