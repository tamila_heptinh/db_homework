use AdventureWorks2016CTP3
go


begin transaction

	select [LastName] 
	from [Person].[Person] 
	where [BusinessEntityID] = 5

	waitfor delay '00:00:10'

	select [LastName] 
	from [Person].[Person] 
	where [BusinessEntityID] = 5

	commit transaction


	update [Person].[Person] 
	set [LastName] = 'Erickson' 
	where [BusinessEntityID] = 5