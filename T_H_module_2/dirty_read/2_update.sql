use AdventureWorks2016CTP3
go

begin transaction
	update [Person].[Person] 
	set [LastName] = 'Stevenson'
	where [FirstName] = 'Michael'

	waitfor delay '00:00:05'

rollback transaction