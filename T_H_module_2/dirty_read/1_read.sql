use AdventureWorks2016CTP3
go

set transaction isolation level read uncommitted
go

begin transaction
	select [LastName] 
	from [Person].[Person] 
	where [FirstName] = 'Michael'
	---- this transaction reads uncomitted data

commit transaction