use AdventureWorks2016CTP3
go

begin transaction

select [BusinessEntityID], [LastName] 
from [Person].[Person]

update [Person].[Person] 
set [LastName] = 'Lee' 
where [BusinessEntityID] = 285

commit transaction