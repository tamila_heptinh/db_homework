use AdventureWorks2016CTP3
go

begin transaction

update [Person].[Person] 
set [LastName] = 'Green' 
where [BusinessEntityID] = 285

--wait till the second transaction reads data

waitfor delay '00:00:05'

commit transaction